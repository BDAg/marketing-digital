#### apoio
# https://www.crummy.com/software/BeautifulSoup/bs4/doc/
# https://imasters.com.br/back-end/aprendendo-sobre-web-scraping-em-python-utilizando-beautifulsoup
# https://medium.com/horadecodar/como-fazer-webscraping-com-python-e-beautiful-soup-28a65eee2efd
# https://www.digitalocean.com/community/tutorials/como-fazer-scraping-em-paginas-web-com-beautiful-soup-and-python-3-pt

import requests
import pandas as pd
from bs4 import BeautifulSoup

page = requests.get("https://www.noticiasagricolas.com.br/noticias/agronegocio/")

soup = BeautifulSoup(page.text,'lxml')

lista = soup.find_all('li', class_='horizontal')

noticias = []
links = []
datas =  []

for noticia in lista:
    noticias.append(noticia.find('h2').get_text())
    datas.append(noticia.find(class_='hora').get_text())
    links.append('https://www.noticiasagricolas.com.br'+noticia.find('a').get('href'))
    
df = pd.DataFrame({
    "noticia": noticias,
    "link": links,
    "data": datas

})

df.head()

#df.to_csv('linkshoje.csv')
